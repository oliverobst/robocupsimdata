# see2csv: Implementations in this directory #

The *.rcv files we create contain the "see" messages that are sent
from simulator to each player. Normally, a match will result in 22
.rcv files (one for each player).

Implementations in this directory are concerned with translating the
rsv files into csv files.

* see2csv.py:

The main script taking care of translating rcv files into csv (needs python3). 
It produces up to two csv output files. 

 * one file containing the landmarks that a player can see. Landmark
   names are taken from our global landmarks file, Flags.csv. If the
   global landmarks file is not given / found, this csv file is not
   created.

 * one file containing the moving objects (the other players and the
   ball) that a player can see. By default, the script will try to determine 
   the order of teams from the input file name. If that fails, the order in 
   which the teams appear in the csv file depends on the order in which players
   see other players. This behaviour can be changed with command line options.

Check out `see2csv.py --help` for more information.


***

older versions in R (only in bitbucket, not recommended)

* see2csv.R:
 
A full R implementation to translate each rcv file into two csv files.
This script can also be run from the command line using Rscript, e.g.,
```
Rscript see2csv.R -i 20170823171452-Gliders2016_0-vs-HELIOS2016_0-Gliders2016_1.rcv 

Rscript see2csv.R --help
```

* flags2csv.R:

Create a file containing landmarks that a player can see. Doesn't need
the global landmark file. 
Should be run using Rscript, e.g.
```
Rscript flags2csv.R --help
```

* moving2csv.R:

create a file containing moving objects that a player can see. 
```
Rscript moving2csv.R --help
```

The R versions take quite long to run (about 25mins per output file,
ie., `see2csv.R` will run for about 50mins on one input file).
The main reason for this seems to be the memory management in R, not
the string matching (based on some profiling). The python version is much
faster (2 secs).
