#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
see2csv.py: Extract moving objects from player 'visual' logs
            into CSV tables. Part of the RoboCupSimData project.

Copyright (C) 2017 
Olivia Michael, Oliver Obst, Falk Schmidsberger, Frieder Stolzenburg

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Created on Sat Sep  2 13:54:59 2017

@author: oliver
"""

import re
import numpy as np
import csv
import argparse
import sys
import textwrap
import os.path

class RCVProcessor:
    def __init__(self, inputfile, landmarksfile, 
                 movingcsv=None, flagscsv=None, team1=None, team2=None):
        self.inputfile = inputfile
        self.movingcsv = movingcsv
        self.flagscsv = flagscsv
        
        try:
            with open(inputfile) as f:
                self.textlog = f.readlines()
                self.textlog = [l.strip() for l in self.textlog]
        except FileNotFoundError:
            print('Input file "' + inputfile + '" not found, exiting.')
            sys.exit(1)
        
        try:
            with open(landmarksfile) as f:
                self.flags = [l['Flags'] for l in csv.DictReader(f, delimiter=',')]
                self.flags = self.flags + list(['F', 'G', 'L'])
        except (FileNotFoundError, TypeError):
            self.flags = None
        
        # set up tables for moving objects and landmarks
        mdtypes = 'int16, ' + ', '.join(['float32'] * 4) + (', ' + ', '.join(['float32'] * 7) + ', U5') * 22 + (', ' + ', '.join(['float32'] * 3) + ', U5') * 22 + ', ' + ', '.join(['float32'] * 44)
        self.movingdata = np.empty(self.num_lines(), dtype = mdtypes)
        self.movingdata[:] = np.NAN
        if self.flags:
            fdtypes = 'int16, ' + ', '.join(['float32'] * (len(self.flags)*4))
            self.flagsdata = np.empty(self.num_lines(), dtype = fdtypes)
            self.flagsdata[:] = np.NAN        
            
        # information for output files: file names and formats
        basenamelist =  os.path.basename(inputfile).split('.')
        if len(basenamelist) > 1 and basenamelist[-1] == 'gz':
            basenamelist = basenamelist[:-1]
        if len(basenamelist) > 1:
            basenamelist = basenamelist[:-1]            
        if movingcsv is None:
            self.movingcsv = '.'.join(basenamelist) + '-moving.csv'
        if flagscsv is None:
            self.flagscsv = '.'.join(basenamelist) + '-landmarks.csv'

        typemap = { 'int16' : '%1d', 'float32' : '%1.3G', 'U5' : '"%.5s"'}
        self.mdformat = [typemap[s] for s in mdtypes.split(', ')]
        if self.flags:
            self.fdformat = [typemap[s] for s in fdtypes.split(', ')]

        # compile regular expressions for parsing the input file
        self.crx_time = re.compile(r'\(see ([0-9]+) ')
        self.crx_time_only = re.compile(r'\(see ([0-9]+)\)')
        self.crx_players = re.compile(r'\(\((?:p|P) ?([^\)]*")? ?([0123456789]*)( goalie)?\) ([- 0123456789\.]*\d) ?([kt]?)\)')
        self.crx_flags = re.compile(r'\(\(((?:g|f|l|G|F|L)[^\)]*)\) ([^\)]+)\)')
        self.crx_ball = re.compile(r'\(\((?:b|B)\) ([- 0123456789\.]*)\)')

        self.team1 = team1
        self.team2 = team2
            
        self.ib0 = 1 # start pos for ball pose
        self.ip0 = 5 # known players t1
        self.ip1 = 93 # known players t2
        self.ip2 = 181 # unknown players t1
        self.ip3 = 225 # unknown players t2
        self.ip4 = 269 # start pos for unknown players
        
        self.max_unknown_t1 = 0
        self.max_unknown_t2 = 0
        self.max_rumsfelds = 0
        
    def num_lines(self):
        return len(self.textlog)
    
    def nmcols(self):
        # time, ball (4 cols), 
        # team1 known players (11 x 8 cols), team2 known players (11 x 8 cols), 
        # team1 unknown players (11x4 cols), team2 unknown players (11x4 cols), 
        # unknown players (22 x 2 cols),  resulting in 313 columns
        return 1 + 4 + 22*8 + 22*4 + 22*2
    
    def nfcols(self):
        # time, 4 columns per flag name (dist, angle, Ddist, Dangle), plus 4 columns each for close F/G/L  
        return 1 + len(self.flags) * 4 if self.flags else 0
    
    def set_column_names(self):
        features2 = ['dist', 'angle']
        features4 = features2 + ['Ddist', 'Dangle']
        features4p = features2 + ['pointdir', 'state']
        features8 = features4 + ['body', 'head', 'pointdir', 'state']
        
        known1 = ['p ' + self.team1 + ' ' + str(i) for i in range(1,12)]
        known2 = ['p ' + self.team2 + ' ' + str(i) for i in range(1,12)]
        unknown1 = ['u ' + self.team1 + ' ' + str(i) for i in range(1,12)]
        unknown2 = ['u ' + self.team2 + ' ' + str(i) for i in range(1,12)]
        unknown_unknown = ['U ' + str(i) for i in range(1,23)]
        
        f0 = ['time']
        f1 = ['b ' + s for s in features4]
        f2 = [s + ' ' + t for s in known1 for t in features8]
        f3 = [s + ' ' + t for s in known2 for t in features8]
        f4 = [s + ' ' + t for s in unknown1 for t in features4p]
        f5 = [s + ' ' + t for s in unknown2 for t in features4p]
        f6 = [s + ' ' + t for s in unknown_unknown for t in features2]
        f = f0 + f1 + f2 + f3 + f4 + f5 + f6
        self.movingdata.dtype.names = f
        
        if self.flags:
            f1 = [s + ' ' + t for s in self.flags for t in features4]
            f = f0 + f1
            self.flagsdata.dtype.names = f
        
        
    def save(self):
        self.set_column_names()
        # remove unused moving columns
        used = list(range(0, self.ip2)) \
            + list(range(self.ip2, self.ip2 + 4*self.max_unknown_t1)) \
            + list(range(self.ip3, self.ip3 + 4*self.max_unknown_t2)) \
            + list(range(self.ip4, self.ip4 + 2*self.max_rumsfelds))
        columns = [self.movingdata.dtype.names[i] for i in used]
        movingdata = self.movingdata[ columns ]
        mdformat = ','.join([self.mdformat[i] for i in used])
        # save moving objects csv file
        np.savetxt(self.movingcsv, movingdata, fmt=mdformat, 
                   header=','.join(movingdata.dtype.names))
        # save flags objects csv file
        if (self.flags):
            fdformat = ','.join(self.fdformat)
            np.savetxt(self.flagscsv, self.flagsdata, fmt=fdformat,
                       header=','.join(self.flagsdata.dtype.names))
    
    def process(self):
        for l in range(0, self.num_lines()):
            # time
            try:
                time = int(self.crx_time.match(self.textlog[l]).group(1))
            except AttributeError:
                # in rare cases the see message is completely empty
                time = int(self.crx_time_only.match(self.textlog[l]).group(1))
                self.movingdata[l][0] = time
                if self.flags:
                    self.flagsdata[l][0] = time
                continue

            self.movingdata[l][0] = time
            # match all landmarks
            if self.flags:
                self.flagsdata[l][0] = time
                for match in self.crx_flags.finditer(self.textlog[l]):
                    j = 4 * self.flags.index(match.group(1))
                    pose = [np.float32(s) for s in match.group(2).split(' ')]
                    for i in range(0, len(pose)):
                        self.flagsdata[l][j + i + 1] = pose[i]
            # match ball
            match = self.crx_ball.search(self.textlog[l])
            if match is not None:
                pose = [np.float32(s) for s in match.group(1).split(' ')]
                for i in range(0, len(pose)):
                    self.movingdata[l][self.ib0 + i] = pose[i]
            # match all players
            unknowns1 = 0 # unknowns team1
            unknowns2 = 0 # unknowns team2
            rumsfelds = 0 # the unknown unknowns
            for match in self.crx_players.finditer(self.textlog[l]):
                if match.group(1) is None: # no teamname: unknown unknown
                    j = self.ip4 + rumsfelds * 2
                    rumsfelds = rumsfelds + 1
                    pose = [np.float32(s) for s in match.group(4).split(' ')]
                    for i in range(0, len(pose)):
                        self.movingdata[l][j + i] = pose[i]
                else:
                    # we have a teamname, now let's see which one
                    isTeam1 = False
                    if self.team1 is None and not match.group(1) == self.team2:
                        self.team1 = match.group(1)
                        isTeam1 = True
                    elif self.team2 is None and not match.group(1) == self.team1:
                        self.team2 = match.group(1)
                    if isTeam1 or self.team1 == match.group(1):
                        isTeam1 = True
                        if len(match.group(2)) == 0:
                            j = self.ip2 + unknowns1 * 4
                            k = 3
                            unknowns1 = unknowns1 + 1
                        else:
                            j = self.ip0 + (int(match.group(2)) - 1) * 8
                            k = 7
                    elif self.team2 == match.group(1):
                        if len(match.group(2)) == 0:
                            j = self.ip3 + unknowns2 * 4
                            k = 3
                            unknowns2 = unknowns2 + 1
                        else:
                            j = self.ip1 + (int(match.group(2)) - 1) * 8
                            k = 7
                    else: # houston we have 3 teams
                        print('Three team names found: ' 
                              + ', '.join([self.team1, self.team2, match.group(1)]))
                        sys.exit(2)
                    # extract pose, 2, 3, 6 or 7 numbers
                    pose = [np.float32(s) for s in match.group(4).split(' ')]
                    if len(pose) % 2 == 1:
                        self.movingdata[l][j + k - 1] = pose[-1]
                        pose = pose[:-1]
                    for i in range(0, len(pose)):
                        self.movingdata[l][j + i] = pose[i]
                    if match.group(3) is None:
                        self.movingdata[l][j + k] = match.group(5)
                    else:
                        self.movingdata[l][j + k] = match.group(5) + 'g'
                        
            self.max_unknown_t1 = max(self.max_unknown_t1, unknowns1)
            self.max_unknown_t2 = max(self.max_unknown_t2, unknowns2)
            self.max_rumsfelds = max(self.max_rumsfelds, rumsfelds)
            
parser = argparse.ArgumentParser(description='translate rcv file into two csv files',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog=textwrap.dedent('''\
If the output file names are not specified, they will be derived from the 
input file name. If no landmarks file is specified (or found), the FLAGSCSV 
file will not be produced.
'''))
parser.add_argument('rcvfile', help='the input file to process (usually .rcv)')
parser.add_argument('-v', '--version', action='version', 
                    help='display version info and exit',
                    version='%(prog)s 1.0: ' + """\
Extract moving objects from player 'visual' logs
                into CSV files. Part of the RoboCupSimData project.

Copyright (C) 2017 
Olivia Michael, Oliver Obst, Falk Schmidsberger, Frieder Stolzenburg

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.                          
    """                    
                    )
parser.add_argument('-l', '--landmarks', 
                    help="the (global) landmarks file, to determine recognised landmark names (e.g., Flags.csv). If not given, no 'flags' output will be produced")
parser.add_argument('--o1', metavar='MOVINGCSV',
                    help='the output file name for the CSV file containing moving objects')
parser.add_argument('--o2', metavar='FLAGSCSV', 
                    help='the output file name for the CSV file containing flags and other landmarks')
parser.add_argument('--no-filename-parsing', dest='filename_parsing', action='store_false',
                    help='do not guess team names from the input file name')
parser.add_argument('--team1', help='the name of the left team (for ordering of the output columns). Using an unused (third) team name will result in an error.')
args = parser.parse_args()

team1 = None
team2 = None
if args.filename_parsing:
    # matching the two team names (characters before the score information)
    # The expected file name syntax is:
    #  date_team1_score1-vs-team2_score2-team1or2_unum.rcv
    m=re.match(r'[0-9]+-(.+)_[0-9]+-vs-(.+)_[0-9]+-.*\..+', 
               os.path.basename(args.rcvfile))
    if m:
        team1 = '"' + m.group(1) + '"'
        team2 = '"' + m.group(2) + '"'
        
if args.team1:
    if args.team1[0] == args.team1[-1] == '"':
        team1 = args.team1
    else:
        team1 = '"' + args.team1 + '"'
    team2 = None

myrcv = RCVProcessor(args.rcvfile, args.landmarks, 
                     args.o1, args.o2, team1, team2)
myrcv.process()
myrcv.save()
sys.exit(0)
