// -*-c++-*-

/***************************************************************************
                                sender.h
                           Base for sender classes
                             -------------------
    begin                : 2002-10-07
    copyright            : (C) 2002 by The RoboCup Soccer Simulator
                           Maintenance Group.
    email                : sserver-admin@lists.sourceforge.net
***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU LGPL as published by the Free Software  *
 *   Foundation; either version 3 of the License, or (at your option) any  *
 *   later version.                                                        *
 *                                                                         *
 ***************************************************************************/


#ifndef RCSSSENDER_H
#define RCSSSENDER_H

#include <iosfwd>
#include <fstream>
#include <cstdio>

namespace rcss {

/*!
//===================================================================
//
//  CLASS: Sender
//
//  DESC: Base sender.
//
//===================================================================
*/

class Sender {
private:
    std::ostream & M_transport;

  bool M_logActive;
  std::ofstream M_logfile;
  std::string M_logname;

protected:
    Sender( std::ostream & transport )
      : M_transport( transport ),
        M_logActive(false)
      { }

    std::ostream & transport()
      {
          return M_transport;
      }

  std::ofstream & logfile()
  {
    return M_logfile;
  }

  void startLog(const std::string & logname)
  {
    M_logfile.open(logname.c_str(), std::ofstream::out);
    M_logActive = M_logfile.is_open();
    M_logname = logname;
  }

  void endLog()
  {
    if (M_logActive) M_logfile.close();
    M_logActive = false;
  }

  void endLog(const std::string & logname)
  {
    if (M_logActive) 
    {
	M_logfile.close();
	std::rename(M_logname.c_str(), logname.c_str());
    }
    M_logActive = false;
  }

  bool isLogActive() const { return M_logActive; }

public:
    virtual
    ~Sender()
      { }

};

}

#endif
