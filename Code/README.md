# Code Section #

3 different pieces of code are part of this project.

## rcg2csv (C++)
Convert the simulator's rcg "logfiles" into csv files. These files
contain the ground truth; there is one rcg file for each game. rcg2csv
also works with gzipped files.  The simulator will also create .rcl
files (containing commands of each robot in plain text).

    # Usage:  rcg2csv [-p] [-d outdir] <RcgFile>[.gz]
    Options:
    -p : is a flag that tells rcg2csv to also store game parameter table 
         (off by default)
    -d outdir : is a command line option to set the output directory 
              (current directory by default)

Compiling rcg2csv.cpp:

- prerequisites
    - boost (tested using boost 1.61.0) 
    - librcsc_gz and librcsc_rcg   
          These are part of the open source librcsc libraries, 
          available at http://osdn.net/projects/rctools/releases/p3777
- edit the Makefile in the rcg2csv directory, to specify the location of boost and the rcsc libraries. 
- make

    


## rcssserver-patch (C++)
This is our code to modify the RoboCup simulator to also log each
players visual and "body" sensors. The patch will allow recording of
this information; it will result in 2 recorded files per player (i.e.,
usually 22 files for a game). The files are recorded in the original
(LISP-style) simulator format.

Usage:
  To switch on logging of visual and body sensors, the following 
  options should be set in the server.conf configuration file. 

- - - 
```
    server::body_log_dated = true
    server::body_log_fixed = false
    server::body_logging = true
     
    server::visuals_log_dated = true
    server::visuals_log_fixed = false
    server::visuals_logging = true
    
    server::body_log_dir = './'
    server::body_log_fixed_name = 'rcssserver'
    server::visuals_log_dir = './'
    server::visuals_log_fixed_name = 'rcssserver'
```
 - - -

  The "_logging" parameters are responsible for switching on or off
  the information being logged. The other parameters are concerned
  with the location of the logfiles ("_dir"), or with the naming of
  the files (all other parameters). The logfile names can be a dated,
  and either contain a string based on team names or based on a given
  fixed name.


Compiling the patch:
  The patch should be applied to the rcssserver software that is 
  available on https://github.com/rcsoccersim/rcssserver   
  Applying the .patch file is sufficient, the other source files
  in this directory are just for completeness. 

  After patching the server, it has to be compiled as usual; the 
  patch doesn't require any additional configuration or dependencies. 


## see2csv (python3)
This is our code to convert players' visual messages into CSV files.
Turning on the visual logging in the simulator will result in a number
of .rcv files being created. The *.rcv files we create contain the
"see" messages that are sent from simulator to each player. Normally,
a match will result in 22 .rcv files (one for each player).

Running `see2csv.py` produces up to two csv output files: 

* one file containing the landmarks that a player can see. Landmark
    names are taken from our global landmarks file, Flags.csv. If the
    global landmarks file is not given / found, this csv file is not
    created.
* one file containing the moving objects (the other players and the
     ball) that a player can see. By default, the script will try to determine 
     the order of teams from the input file name. If that fails, the order in 
     which the teams appear in the csv file depends on the order in which players
     see other players. This behaviour can be changed with command line options.


```
     Usage: see2csv.py [-h] [-v] [-l LANDMARKS] [--o1 MOVINGCSV] [--o2 FLAGSCSV]
                  [--no-filename-parsing] [--team1 TEAM1]
                  rcvfile
    
    positional arguments:
      rcvfile               the input file to process (usually .rcv)
    
    optional arguments:
    -h, --help            show this help message and exit
    -v, --version         display version info and exit
    -l LANDMARKS, --landmarks LANDMARKS
                          the (global) landmarks file, to determine recognised
                          landmark names (e.g., Flags.csv). If not given, no
                          'flags' output will be produced
    --o1 MOVINGCSV        the output file name for the CSV file containing
                          moving objects
    --o2 FLAGSCSV         the output file name for the CSV file containing flags
                          and other landmarks
    --no-filename-parsing
                          do not guess team names from the input file name
    --team1 TEAM1         the name of the left team (for ordering of the output
                          columns). Using an unused (third) team name will
                          result in an error.
```

If the output file names are not specified, they will be derived from the 
input file name. If no landmarks file is specified (or found), the FLAGSCSV 
file will not be produced. There is a landmarks file that comes with the 
software.

