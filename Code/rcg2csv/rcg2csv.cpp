/** -*-c++-*-
rcg2csv: Convert RoboCup Soccer Simulator rcg files to CSV.
Copyright (C) 2017 
O. Michael, O. Obst, F. Schmidsberger, F. Stolzenburg

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
*/ 

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_NETINET_IN_H
#include <netinet/in.h>
#endif

#include <cmath>
#include <cstring>
#include <iostream>
#include <fstream>
#include <string>
#include <libgen.h>
#include <rcsc/gz.h>
#include <rcsc/rcg.h>

class TextPrinter
    : public rcsc::rcg::Handler {
private:
    struct CommandCount {
        int kick_;
        int dash_;
        int turn_;
        int say_;
        int turn_neck_;
        int catch_;
        int move_;
        int change_view_;

        CommandCount()
            : kick_( 0 )
            , dash_( 0 )
            , turn_( 0 )
            , say_( 0 )
            , turn_neck_( 0 )
            , catch_( 0 )
            , move_( 0 )
            , change_view_( 0 )
          { }
        void update( const rcsc::rcg::player_t & player )
          {
              kick_ = rcsc::rcg::nstohi( player.kick_count );
              dash_ = rcsc::rcg::nstohi( player.dash_count );
              turn_ = rcsc::rcg::nstohi( player.turn_count );
              say_ = rcsc::rcg::nstohi( player.say_count );
              turn_neck_ = rcsc::rcg::nstohi( player.turn_neck_count );
              catch_ = rcsc::rcg::nstohi( player.catch_count );
              move_ = rcsc::rcg::nstohi( player.move_count );
              change_view_ = rcsc::rcg::nstohi( player.change_view_count );
          }
        void update( const rcsc::rcg::PlayerT & player )
          {
              kick_ = player.kick_count_;
              dash_ = player.dash_count_;
              turn_ = player.turn_count_;
              say_ = player.say_count_;
              turn_neck_ = player.turn_neck_count_;
              catch_ = player.catch_count_;
              move_ = player.move_count_;
              change_view_ = player.change_view_count_;
          }
    };

    std::ostream & M_ps;
    std::ostream & M_os;

    bool M_init_written;
    bool M_header_written;
    rcsc::PlayMode M_playmode;
    std::string M_left_team_name;
    std::string M_right_team_name;
    int M_left_score;
    int M_right_score;
    std::string M_sep;
    std::string M_NA;

    CommandCount M_command_count[rcsc::MAX_PLAYER * 2];

    // not used
    TextPrinter();
public:

    explicit
    TextPrinter( std::ostream & os0, std::ostream & os1 );

    // v3 or older
    bool handleDispInfo( const rcsc::rcg::dispinfo_t & disp );
    bool handleShowInfo( const rcsc::rcg::showinfo_t & show );
    bool handleShortShowInfo2( const rcsc::rcg::short_showinfo_t2 & show );
    bool handleMsgInfo( rcsc::rcg::Int16,
                        const std::string & )
      {
          return true;
      }
    bool handlePlayMode( char playmode );
    bool handleTeamInfo( const rcsc::rcg::team_t & team_left,
                         const rcsc::rcg::team_t & team_right );
    bool handlePlayerType( const rcsc::rcg::player_type_t & param );
    bool handleServerParam( const rcsc::rcg::server_params_t & param );
    bool handlePlayerParam( const rcsc::rcg::player_params_t & param );

    // common
    bool handleEOF();

    // v4 or later
    bool handleShow( const int time,
                     const rcsc::rcg::ShowInfoT & show );
    bool handleMsg( const int time,
                    const int board,
                    const std::string & msg );
    bool handlePlayMode( const int time,
                         const rcsc::PlayMode pm );
    bool handleTeam( const int time,
                     const rcsc::rcg::TeamT & team_l,
                     const rcsc::rcg::TeamT & team_r );
    bool handleServerParam( const std::string & msg );
    bool handlePlayerParam( const std::string & msg );
    bool handlePlayerType( const std::string & msg );

private:
    const
    std::string & getPlayModeString( const rcsc::PlayMode playmode ) const;


    std::ostream & printState( std::ostream & os,
                               const long & cycle ) const;

    std::ostream & printBall( std::ostream & os,
                              const rcsc::rcg::pos_t & ball ) const;
    std::ostream & printBall( std::ostream & os,
                              const rcsc::rcg::ball_t & ball ) const;

    std::ostream & printPlayer( std::ostream & os,
                                const rcsc::rcg::pos_t & player ) const;
    std::ostream & printPlayer( std::ostream & os,
                                const rcsc::rcg::player_t & player ) const;

    std::ostream & printTableHeader( std::ostream & os, 
				     const rcsc::rcg::showinfo_t & show ) const;
    std::ostream & printTableHeader( std::ostream & os, 
				     const rcsc::rcg::short_showinfo_t2 & show ) const;

    std::ostream & printTableHeader( std::ostream & os, const rcsc::rcg::ShowInfoT & show ) const;

    void writeParamFile(bool addTeamnames);

    void writeParamFile(bool addTeamnames, const std::string & params);

};


/*-------------------------------------------------------------------*/
/*!

 */
TextPrinter::TextPrinter( std::ostream & os0, std::ostream & os1 )
    : M_ps( os0 )
    , M_os( os1 )
    , M_init_written( false )
    , M_header_written( false )
    , M_playmode( rcsc::PM_Null )
    , M_left_team_name( "" )
    , M_right_team_name( "" )
    , M_left_score( 0 )
    , M_right_score( 0 )
    , M_sep(", ")
    , M_NA("NA")
{

}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleDispInfo( const rcsc::rcg::dispinfo_t & disp )
{
    //std::cerr << "handleDispInfo size = " << sizeof( disp ) << std::endl;
    switch ( ntohs( disp.mode ) ) {
    case rcsc::rcg::SHOW_MODE:
        //std::cerr << "SHOW_MODE" << std::endl;
        handleShowInfo( disp.body.show );
        break;
    case rcsc::rcg::MSG_MODE:
        //td::cerr << "MSG_MODE" << std::endl;
        break;
    case rcsc::rcg::DRAW_MODE:
        //std::cerr << "DRAW_MODE" << std::endl;
        break;
    case rcsc::rcg::BLANK_MODE:
        //std::cerr << "BLANK_MODE" << std::endl;
        break;
    case rcsc::rcg::PM_MODE:
        //std::cerr << "PM_MODE" << std::endl;
        break;
    case rcsc::rcg::TEAM_MODE:
        //std::cerr << "TEAM_MODE" << std::endl;
        break;
    case rcsc::rcg::PT_MODE:
        //std::cerr << "PT_MODE" << std::endl;
        break;
    case rcsc::rcg::PARAM_MODE:
        //std::cerr << "PARAM_MODE" << std::endl;
        break;
    case rcsc::rcg::PPARAM_MODE:
        //std::cerr << "PPARAM_MODE" << std::endl;
        break;
    default:
        std::cerr << __FILE__ << ":" << __LINE__
                  << " Unsupported mode " << ntohs( disp.mode )
                  << " in dispinfo_t" << std::endl;
        return false;
        break;
    }

    return true;
}

std::ostream & 
TextPrinter::printTableHeader( std::ostream & os, const rcsc::rcg::showinfo_t & show ) const
{
    os << "showinfo_t time" << M_sep << "playmode" 
       << M_sep << "score left" << M_sep << "score right"
       << M_sep << "ball_x" << M_sep << "ball_y" 
       << M_sep << "ball_vx" << M_sep << "ball_vy";

    for ( int i = 0; i < rcsc::MAX_PLAYER*2; ++i )
    {
	const short mode = ntohs( show.pos[i].enable );
	const std::string & teamname = ( i <= rcsc::MAX_PLAYER
					 ? M_left_team_name
					 : M_right_team_name );
	int unum = ( i <= rcsc::MAX_PLAYER
		     ? i + 1
		     : i + 1 - rcsc::MAX_PLAYER );
	    
	os << M_sep << "\"" << teamname << "\" " << unum;
	if ( mode & rcsc::rcg::GOALIE )
	{
	    os << " goalie";
	}
    }
    
    os  << std::endl;	
    return os;
}

std::ostream & 
TextPrinter::printTableHeader( std::ostream & os, const rcsc::rcg::short_showinfo_t2 & show ) const
{
    os << "showinfo_t2 time" << M_sep << "playmode" 
       << M_sep << "score left" << M_sep << "score right"
       << M_sep << "ball_x" << M_sep << "ball_y" 
       << M_sep << "ball_vx" << M_sep << "ball_vy";
    for ( int i = 0; i < rcsc::MAX_PLAYER*2; ++i )
    {
	const short mode = ntohs( show.pos[i].mode );
	std::string team;
	if ( mode & rcsc::rcg::GOALIE )
	{
	    team = ( i < rcsc::MAX_PLAYER ? "LG" : "RG" );
	}
	else
	{
	    team = ( i < rcsc::MAX_PLAYER ? "L" : "R" );
	}

	int unum = ( i < rcsc::MAX_PLAYER
		     ? i + 1
		     : i + 1 - rcsc::MAX_PLAYER );
	    
	os << M_sep << team << unum << " x"
	   << M_sep << team << unum << " y"
	   << M_sep << team << unum << " vx"
	   << M_sep << team << unum << " vy"
	   << M_sep << team << unum << " body"
	   << M_sep << team << unum << " head"
	   << M_sep << team << unum << " vieww"
	   << M_sep << team << unum << " viewq";
    }
    
    os  << std::endl;	
    return os;
}

std::ostream & 
TextPrinter::printTableHeader( std::ostream & os, const rcsc::rcg::ShowInfoT & show ) const
{
    os << "time" << M_sep << "playmode" 
       << M_sep << "score left" << M_sep << "score right"
       << M_sep << "ball_x" << M_sep << "ball_y" 
       << M_sep << "ball_vx" << M_sep << "ball_vy";

    for ( int i = 0; i < rcsc::MAX_PLAYER*2; ++i )
    {
	rcsc::rcg::player_t p;
	rcsc::rcg::Serializer::convert( show.player_[i], p );
	const short mode = ntohs( p.mode );
	std::string team;
	if ( mode & rcsc::rcg::GOALIE )
	{
	    team = ( i < rcsc::MAX_PLAYER ? "LG" : "RG" );
	}
	else
	{
	    team = ( i < rcsc::MAX_PLAYER ? "L" : "R" );
	}

	int unum = ( i < rcsc::MAX_PLAYER
		     ? i + 1
		     : i + 1 - rcsc::MAX_PLAYER );
	    
	os << M_sep << team << unum << " x"
	   << M_sep << team << unum << " y"
	   << M_sep << team << unum << " vx"
	   << M_sep << team << unum << " vy"
	   << M_sep << team << unum << " body"
	   << M_sep << team << unum << " head"
	   << M_sep << team << unum << " vieww"
	   << M_sep << team << unum << " viewq";
    }
    
    os  << std::endl;	
    return os;
}


void
TextPrinter::writeParamFile(bool addTeamnames)
{
    if (addTeamnames)
    {
	M_ps << "Left" << M_sep << M_left_team_name << "\n"
	     << "Right" << M_sep << M_right_team_name << "\n";
    }
    else 
    {

    }
}

void 
TextPrinter::writeParamFile(bool addTeamnames, const std::string & params)
{
    if (addTeamnames)
    {
	M_ps << "Left" << M_sep << M_left_team_name << "\n"
	     << "Right" << M_sep << M_right_team_name << "\n";
    }
    else 
    {
	//	M_os << "xxx" << params << "\n";
	size_t i = params.find('(');
	size_t j = params.find(' ', i) - i - 1;
	while (i != std::string::npos)
	{
	    ++i;
	    M_ps << params.substr(i,j) << M_sep;
	    i = i + j + 1;
	    j = params.find(')', i) - i;
	    M_ps << params.substr(i,j) << "\n";
	    i = params.find('(', i);
	    j = params.find(' ', i) - i - 1;
	}
    }
}


/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleShowInfo( const rcsc::rcg::showinfo_t & show )
{
    if ( ! M_init_written )
    {
        writeParamFile(false);
        M_init_written = true;
    }

    if ( ! M_header_written )
    {
	M_header_written = true;
	writeParamFile(M_init_written);
	printTableHeader( M_os, show );
    }

    handlePlayMode( show.pmode );
    handleTeamInfo( show.team[0], show.team[1] );

    printState( M_os, static_cast< long >( static_cast< short >( ntohs( show.time ) ) ) );
    M_os << ' ';
    printBall( M_os, show.pos[0] );

    for ( int i = 1; i < rcsc::MAX_PLAYER * 2 + 1; ++i )
    {
        if ( ntohs( show.pos[i].enable ) == 0 )
        {
            // player is not connected
            continue;
        }

        M_os << ' ';
        printPlayer( M_os, show.pos[i] );
    }

    M_os << ")" << std::endl;
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleShortShowInfo2( const rcsc::rcg::short_showinfo_t2 & show )
{
    if ( ! M_init_written )
    {
        M_init_written = true;
	//	M_os << "Left" << M_sep << M_left_team_name << "\n"
	//	     << "Right" << M_sep << M_right_team_name << "\n";
    }

    if ( ! M_header_written )
    {
	M_header_written = true;
	writeParamFile( M_init_written );
	printTableHeader( M_os, show );
    }

    M_os << "(Info ";
    printState( M_os, static_cast< long >( static_cast< short >( ntohs( show.time ) ) ) );
    M_os << " ";
    printBall( M_os, show.ball );

    for ( int i = 0; i < rcsc::MAX_PLAYER * 2; ++i )
    {
        if ( ntohs( show.pos[i].mode ) == 0 )
        {
            // player is not connected
            continue;
        }

        M_os << " ";
        printPlayer( M_os, show.pos[i] );
        M_command_count[i].update( show.pos[i] );
    }

    M_os << ")" << std::endl;
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayMode( char playmode )
{
    M_playmode = static_cast< rcsc::PlayMode >( playmode );
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleTeamInfo( const rcsc::rcg::team_t & team_left,
                             const rcsc::rcg::team_t & team_right )
{
    if ( M_left_team_name.empty() )
    {
        char buf[18];
        std::memset( buf, '\0', 18 );
        std::strncpy( buf, team_left.name, 16 );
        M_left_team_name = buf;
    }
    if ( M_right_team_name.empty() )
    {
        char buf[18];
        std::memset( buf, '\0', 18 );
        std::strncpy( buf, team_right.name, 16 );
        M_right_team_name = buf;
    }

    M_left_score = rcsc::rcg::nstohi( team_left.score );
    M_right_score = rcsc::rcg::nstohi( team_right.score );

    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayerType( const rcsc::rcg::player_type_t & )
{
    std::cerr << "handlePlayerType" << std::endl;
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleServerParam( const rcsc::rcg::server_params_t & param )
{
    M_init_written = true;
    M_ps << "Left" << M_sep << M_left_team_name << "\n"
	 << "Right" << M_sep << M_right_team_name << "\n";

    M_ps << "(Inix"
         << " (goal_width " << rcsc::rcg::nltohd( param.goal_width ) << ")"
         << " (player_size " << rcsc::rcg::nltohd( param.player_size ) << ")"
         << " (ball_size " << rcsc::rcg::nltohd( param.ball_size ) << ")"
         << " (kickable_margin " << rcsc::rcg::nltohd( param.kickable_margin ) << ")"
         << " (visible_distance " <<  rcsc::rcg::nltohd( param.visible_distance ) << ")"
         << " (kickable_area " << ( rcsc::rcg::nltohd( param.player_size )
                                    + rcsc::rcg::nltohd( param.ball_size )
                                    + rcsc::rcg::nltohd( param.kickable_margin ) )
         << ")"
         << " (catchable_area_l " << rcsc::rcg::nltohd( param.catch_area_l ) << ")"
         << " (catchable_area_w " << rcsc::rcg::nltohd( param.catch_area_w ) << ")"
         << " (half_time " << static_cast< short >( ntohs( param.half_time ) ) << ")"
         << " (ckick_margin " << rcsc::rcg::nltohd( param.corner_kick_margin ) << ")"
         << " (offside_active_area_size " << rcsc::rcg::nltohd( param.offside_active_area ) << ")"
         << " (offside_kick_margin " << rcsc::rcg::nltohd( param.offside_kick_margin ) << ")"
         << " (audio_cut_dist " << rcsc::rcg::nltohd( param.audio_cut_dist ) << ")"
         << ")"
         << std::endl;

    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayerParam( const rcsc::rcg::player_params_t & )
{
    std::cerr << "handlePlayerParam" << std::endl;
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleEOF()
{
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleShow( const int,
                         const rcsc::rcg::ShowInfoT & show )
{
    if ( ! M_init_written )
    {
        M_init_written = true;
    }

    if ( ! M_header_written )
    {
	M_header_written = true;
	writeParamFile(M_init_written);
	printTableHeader( M_os, show );
    }    

    printState( M_os, show.time_ );

    // ball
    M_os << M_sep << show.ball_.x_ << M_sep << show.ball_.y_ 
	 << M_sep << show.ball_.vx_ << M_sep << show.ball_.vy_;

    // players
    for ( int i = 0; i < rcsc::MAX_PLAYER*2; ++i )
    {
        rcsc::rcg::player_t p;
        rcsc::rcg::Serializer::convert( show.player_[i], p );

        printPlayer( M_os, p );
        M_command_count[i].update( show.player_[i] );
    }

    M_os << "\n";
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleMsg( const int,
                        const int,
                        const std::string & )
{
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayMode( const int,
                             const rcsc::PlayMode pm )
{
    M_playmode = pm;
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleTeam( const int,
                         const rcsc::rcg::TeamT & team_l,
                         const rcsc::rcg::TeamT & team_r )
{
    if ( M_left_team_name.empty() )
    {
        M_left_team_name = team_l.name_;
    }
    if ( M_right_team_name.empty() )
    {
        M_right_team_name = team_r.name_;
    }

    M_left_score = team_l.score_;
    M_right_score = team_r.score_;

    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handleServerParam( const std::string & msg )
{
    std::string::size_type pos = msg.find_first_of( ' ' );
    if ( pos != std::string::npos )
    {
        writeParamFile(M_init_written, msg.substr( pos ));
        M_init_written = true;
    }
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayerParam( const std::string & )
{
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
bool
TextPrinter::handlePlayerType( const std::string & )
{
    return true;
}

/*-------------------------------------------------------------------*/
/*!

 */
const
std::string &
TextPrinter::getPlayModeString( const rcsc::PlayMode playmode ) const
{
    static const std::string s_playmode_str[] = {
        "unknown playmode",
        "before_kick_off",
        "time_over",
        "play_on",
        "kick_off_l",
        "kick_off_r",
        "kick_in_l",
        "kick_in_r",
        "free_kick_l",
        "free_kick_r",
        "corner_kick_l",
        "corner_kick_r",
        "goal_kick_l",
        "goal_kick_r",
        "goal_l",
        "goal_r",
        "drop_ball",
        "offside_l",
        "offside_r",
        "penalty_kick_l",
        "penalty_kick_r",
        "first_half_over",
        "pause",
        "human_judge",
        "foul_charge_l",
        "foul_charge_r",
        "foul_push_l",
        "foul_push_r",
        "foul_multiple_attack_l",
        "foul_multiple_attack_r",
        "foul_ballout_l",
        "foul_ballout_r",
        "back_pass_l",
        "back_pass_r",
        "free_kick_fault_l",
        "free_kick_fault_r",
        "catch_fault_l",
        "catch_fault_r",
        "indirect_free_kick_l",
        "indirect_free_kick_r",
        "penalty_setup_l",
        "penalty_setup_r",
        "penalty_ready_l",
        "penalty_ready_r",
        "penalty_taken_l",
        "penalty_taken_r",
        "penalty_miss_l",
        "penalty_miss_r",
        "penalty_score_l",
        "penalty_score_r",
        ""
    };


    if ( playmode < rcsc::PM_Null
         || rcsc::PM_MAX < playmode )
    {
        return s_playmode_str[0];
    }

    return s_playmode_str[playmode];
}

/*-------------------------------------------------------------------*/
/*!

 */
std::ostream &
TextPrinter::printState( std::ostream & os,
                         const long & cycle ) const
{
    os << cycle << M_sep << getPlayModeString( M_playmode ) 
       << M_sep << M_left_score 
       << M_sep << M_right_score;
    return os;
}

/*-------------------------------------------------------------------*/
/*!
  old log format
*/
std::ostream &
TextPrinter::printBall( std::ostream & os,
                        const rcsc::rcg::pos_t & ball ) const
{
    os << rcsc::rcg::nstohd( ball.x ) 
       << M_sep << rcsc::rcg::nstohd( ball.y )
       << M_sep << M_NA << M_sep << M_NA;

    return os;
}

/*-------------------------------------------------------------------*/
/*!

 */
std::ostream &
TextPrinter::printBall( std::ostream & os,
                        const rcsc::rcg::ball_t & ball ) const
{
    os << rcsc::rcg::nltohd( ball.x )
       << M_sep << rcsc::rcg::nltohd( ball.y )
       << M_sep << rcsc::rcg::nltohd( ball.deltax )
       << M_sep << rcsc::rcg::nltohd( ball.deltay );

    return os;
}


/*-------------------------------------------------------------------*/
/*!
  old log format
*/
std::ostream &
TextPrinter::printPlayer( std::ostream & os,
                          const rcsc::rcg::pos_t & player ) const
{
  //    const short mode = ntohs( player.enable );

    os << M_sep << rcsc::rcg::nstohd( player.x ) 
       << M_sep << rcsc::rcg::nstohd( player.y )
       << M_sep << M_NA << M_sep << M_NA
       << M_sep << static_cast< double >( static_cast< short >( ntohs( player.angle ) ) ) 
       << M_sep << M_NA // head angle not used 
       << M_sep << M_NA  // view width
       << M_sep << M_NA; // view quality

    return os;
}


/*-------------------------------------------------------------------*/
/*!

 */
std::ostream &
TextPrinter::printPlayer( std::ostream & os,
                          const rcsc::rcg::player_t & player ) const
{
  //    const short mode = ntohs( player.mode );

    double body_deg
        = rcsc::rcg::nltohd( player.body_angle )
        * (180.0 / M_PI);
    double head_deg
        = rcsc::rcg::nltohd( player.head_angle )
        * (180.0 / M_PI);
    head_deg += body_deg;
    while ( head_deg > 180.0 )
    {
        head_deg -= 360.0;
    }
    while ( head_deg < -180.0 )
    {
        head_deg += 360.0;
    }

    double view_width 
      = round(rcsc::rcg::nltohd( player.view_width ) * (180.0 / M_PI));
    int view_quality = rcsc::rcg::nstohi( player.view_quality );

    os << M_sep << rcsc::rcg::nltohd( player.x ) 
       << M_sep << rcsc::rcg::nltohd( player.y ) 
       << M_sep << rcsc::rcg::nltohd( player.deltax ) 
       << M_sep << rcsc::rcg::nltohd( player.deltay ) 
       << M_sep << body_deg 
       << M_sep << head_deg
       << M_sep << view_width
       << M_sep << view_quality;

    return os;
}


////////////////////////////////////////////////////////////////////////
void
usage(const std::string & program) 
{
    std::cerr << "Usage: " << program 
	      << " [-p] [-d outdir] <RcgFile>[.gz]\n\n" 
	      << "Options:\n"
	      << " -h\t\tprint this help\n"
	      << " -p\t\tstore game parameter table (off by default)\n"
	      << " -d outdir\toption to set output directory (current directory by default)\n"
	      << std::endl;
}

int
main( int argc, char** argv )
{
    int opt;
    bool writeParams = false;
    bool printUsage = false;
    std::string infile;
    std::string outdir("./");
    std::string myname = basename(argv[0]);

    while ((opt = getopt(argc, argv, "hpd:")) != -1) 
    {
	switch (opt) {
	case 'h':
	    printUsage = true;
	    break;
	case 'p':
	    writeParams = true;
	    break;
	case 'd':
	    outdir = optarg;
	    if (outdir.empty()) outdir = "./";
	    if (outdir.back() != '/') outdir += '/';
	    break;
	default: /* '?' */
	    usage(myname);
	    exit(EXIT_FAILURE);
	}
    }
    argc -= optind;
    argv += optind;

    if ((argc != 1) && ! printUsage) 
    {
	std::cerr << "Expected argument after options\n\n";
	usage(myname);
	exit(EXIT_FAILURE);
    }

    if ( printUsage )
    {
        usage(myname);
        return 0;
    }

    infile = argv[0];

    char pathname[1024];
    std::string base = basename_r(infile.c_str(), pathname);
    std::string suffix = base.substr(base.size()-3);

    if (suffix.compare(".gz") == 0) 
    {
	base = base.substr(0, base.size() - 3);
    }

    suffix = base.substr(base.size() - 4);
    if (suffix.compare(".rcg") == 0)
    {
	base = base.substr(0, base.size() - 4);
    }

    std::string outfile0 = outdir + base + "-pt.csv";
    std::string outfile1 = outdir + base + "-gt.csv";


    rcsc::gzifstream fin( infile.c_str() );
    if ( ! fin.is_open() )
    {
        std::cerr << "Failed to open file : " << infile << std::endl;
        return 1;
    }

    std::ofstream fout0;
    std::ofstream fout1( outfile1 );

    if (writeParams)
    {
	fout0.open( outfile0 );
    }

    rcsc::rcg::Parser::Ptr parser = rcsc::rcg::Parser::create( fin );

    if ( ! parser )
    {
        std::cerr << "Failed to create rcg parser." << std::endl;
        return 1;
    }

    // create rcg handler instance
    TextPrinter printer( fout0, fout1 );

    parser->parse( fin, printer );

    fout0.close();
    fout1.close();

    return 0;
}
