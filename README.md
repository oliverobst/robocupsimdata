# RoboCupSimData 

## Software and Data for Machine Learning from RoboCup Simulation League

With this project, we provide the software necessary to create
datasets for learning in RoboCup Soccer simulation, using existing
teams (binaries). The Soccer Simulator usually records the games as
they are played (i.e., the ground truth data), while the local, noisy,
and restricted information that every player in this simulation gets
is usually lost. Our patches to the simulator add the capability to
record individual percepts for every player, but also the tools to
translate these recordings into CSV files.

We also provide a large dataset from games of some of the top teams
(from 2016 and 2017) in RoboCup Soccer Simulation League (2D), where
teams of 11 robots ("agents") compete against each other. Overall, we
used 10 different teams to play each other, resulting in 45 unique
pairings. For each pairing, we ran 25 matches (of 10 minutes), leading
to 1125 matches or more than 180 hours of game play.  The generated
CSV files are 17GB of data (zipped), or 229GB (unzipped).

The dataset is unique in the sense that it contains both the "ground
truth" data (global, complete, noise-free information of all objects
on the field), as well as the noisy, local and incomplete percepts of
each robot.  These data are made available as CSV files, as well as in
the original soccer simulator formats.

Additionally, the dataset contains logs of all actions of each robot
as received from the simulator, as well as agent and game status
information as received by each agent from the simulator (an extra 25GB
of gzipped data, together with the original simulator logs).

## Using our Software

Recording new data requires the following software:

* The RoboCup Soccer Simulator: https://github.com/rcsoccersim
  We used version 15.3.0.
* Binaries of the 2016 and 2017 2D Simulation League teams: https://archive.robocup.info/Soccer/Simulation/2D/binaries/RoboCup/
* The librcsc library: https://osdn.net/projects/rctools/releases/p3777
* The software and patches that we provide as part of this repository: see the [Code README](/Code/README.md).

## Downloading our Software and Data

Our software is open source: the latest release can be downloaded
[robocupsimdata-1.0.tar.gz](http://oliver.obst.eu/code/RoboCupSimData/robocupsimdata-1.0.tar.gz),
or the current version can also be cloned from this repository. 

The datasets that we created can be 
[downloaded here](http://oliver.obst.eu/data/RoboCupSimData/overview.html).  
We provide packages of different sizes: The complete dataset encompasses
a 25 round tournament between 10 teams. To also provide smaller datasets,
we determined the top 5 teams from this tournament, and selected 10
games from the full dataset, so that each team plays one of the other
top 5 once. Finally, the smallest dataset is just a one game example match.


## Attributions

The relevant paper for the software and dataset is at http://arxiv.org/abs/1711.01703  

O. Michael, O. Obst, F. Schmidsberger, F. Stolzenburg,
RoboCupSimData: Software and Data for machine learning from RoboCup Simulation League, 2017.

Team description papers (of the teams that we used to play the tournament) can be found at https://archive.robocup.info/Soccer/Simulation/2D/TDPs/RoboCup/

## Licenses

The code that we make available with the RoboCupSimData project is released under 
[GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html): https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

Our dataset is made available under the 
[Open Database License (ODbL) v1.0](http://opendatacommons.org/licenses/odbl/1.0/)
http://opendatacommons.org/licenses/odbl/1.0/. Any rights in
individual contents of the database are licensed under the 
[Database Contents License](http://opendatacommons.org/licenses/dbcl/1.0/):
http://opendatacommons.org/licenses/dbcl/1.0/

Please see here for a plain-text (human-readable) summary of the ODbL: https://opendatacommons.org/licenses/odbl/summary/

